import * as React from 'react';

import { default as TodoContainer } from '../Todo/TodoContainer';

class App extends React.Component {
  render() {
    return (
      <TodoContainer />
    );
  }
}

export default App;
