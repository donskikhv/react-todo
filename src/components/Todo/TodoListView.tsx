import * as React from 'react';
import { Props } from 'react';
import { Set } from 'immutable';

import { Task } from '../../models/Task';

interface TodoListViewProps{
  tasks: Set<Task>;
}

const TodoListView: React.SFC<TodoListViewProps> = (props) => {
  return (
    <ul>
      {props.tasks.toArray()
        .map((task, key) => {
          return(
            <li key={key}>
              {task.name}
            </li>
          );
        })
      }
    </ul>
  );
};

export default TodoListView;