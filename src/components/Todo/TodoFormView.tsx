import * as React from 'react';
import { Props } from 'react';
import { Set } from 'immutable';

import { Task } from '../../models/Task';

interface TodoFormViewProps extends Props<TodoFormView> {
  onAdd: (task: Task) => void;
  onDelete: (task: Task) => void;
}

interface TodoFormViewState {
  newTask: Task;
}

class TodoFormView extends React.Component<TodoFormViewProps, TodoFormViewState> {
  state = {
    newTask: {
      name: ''
    }
  };

  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      newTask: {
        name: event.target.value
      }
    });
  }

  render() {
    return(
      <div>
        <input onChange={this.handleChange} />
        <button onClick={() => this.props.onAdd(this.state.newTask)} type='submit'>Add a task</button>
      </div>
    );
  }
  
}

export default TodoFormView;