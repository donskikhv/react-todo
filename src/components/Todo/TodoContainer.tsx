import * as React from 'react';
import { Set } from 'immutable';

import TodoFormView from './TodoFormView';
import TodoListView from './TodoListView';

import { Task } from '../../models/Task';

interface TodoContainerState {
  tasks: Set<Task>;
}

class TodoContainer extends React.Component<{}, TodoContainerState> {
  state = {
    tasks: Set<Task>()
  };

  onAdd(task: Task) {
    this.setState({
      tasks: this.state.tasks.add(task)
    });
  }

  onDelete(task: Task) {
    this.setState({
      tasks: this.state.tasks.remove(task)
    });
  }

  render() {
    return(
      <div>
        <TodoFormView
          onAdd={(task: Task) => this.onAdd(task)}
          onDelete={(task: Task) => this.onDelete(task)}
        />
        <TodoListView tasks={this.state.tasks} />
      </div>
    );
  }
}

export default TodoContainer;